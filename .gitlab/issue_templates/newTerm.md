### About this request

Write a sentence or two about why you think this term should be added.

### Semantic issues

Write a sentence or two about what kind of semantic confusion usually happens around this term. 

### Term 

Add your suggestions for the fields below. You can delete the fields that you don't use, but the following are mandatory:

 * label_en: This is the term you are proposing
 * definition_en: A definition that can be used as a starting point
 * sources: Resources your definition is based on
 * contributors: A way of identifying you and everyone who collaborated, preferably an ORCID

```yaml
--- # Free-text name of definition source 
labels: 
  label_en: text text text
singular: 
  singular_en: text text
plural: 
  plural_en: text
acronyms: 
  acronym_en: 
    - may be a list of acronyms
abbreviations: 
  abbreviation_en: a single abbreviation
    - or a list of abbreviations
synonyms: 
  exact_synonym_en: 
    - text
    - text
    - text
  narrow_synonym_en: 
    - text
    - text
    - text
  broad_synonym_en: 
    - text
    - text
    - text
  related_synonym_en: 
    - text
    - text
    - text
iri: https://iris.hmc-services.de/term_00000001
definitions: 
  definition_en: > 
    Write your definition here, you can add new lines (pressing enter) like this 
    as many times as you like, as the greater-than sign at the top will wrap the 
    text into a single line. Make sure your lines either end or begin with a 
    space (with spacebar).
sources: 
  - https://url.to.my.source.org/
  - https://url.to.my.source.org/
  - https://url.to.my.source.org/
comments: 
  comment_en: > 
    Write your comment here, same formatting rules as definitions.
examples:
  - term example
  - larger term example
  - instance example
seeAlso: 
  - http://interesting-page.com/term
  - http://another-interesting-page.com/term
contributors: 
  - https://orcid.org/Your_ORCID_CODE
  - https://orcid.org/ORCID_CODE_OF_OTHERS_WHO_CONTRIUBUTED
ratified: False
```

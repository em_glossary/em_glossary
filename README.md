# EM Glossary Community Repository

<img src="EMGlossary.png" alt="EM Glossary Group Logo" width="400"/> 

# Welcome to the EM Glossary
We work towards harmonized metadata in electron microscopy!

This repository is used for **content development for the EM Glossary** :wrench:. Development is carried out in a moderated community process including EM domain and method experts, knowledge engineers and metadata stewards :handshake:. 

After content development in this repository the EM Glossary is implemented as: 
1. **machine readible artifact**: see [EM Glossary OWL GitLab](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl) and [emg.owl documentation](owl.emglossary.helmholtz-metadaten.de);
2. **webpage** for interactive browsing. Start at our [LandingPage](emglossary.helmholtz-metadaten.de) and see open source code in our [EM Glossary Web GitLab](https://codebase.helmholtz.cloud/em_glossary/em_glossary_web).

### In this repository:
In this repository you can find: 

| | |
|--|--|
| :file_folder: terms | contains YAML files with developed content - one file per term |
| :file_folder: scripts | python scripts used in our CI/CD pipelines |
| :file_folder: schema | JSON & YAML files defining a schema for the YAML content files used for validation in our CI/CD pipeline |

### Content development workflow
We use this GitLab repository to capture the full provenance of all discussions in our community process.

Our workflows entails:
1. **Term proposition, priorization and initial definition:** for this we use the GitLab [Issue Board](https://codebase.helmholtz.cloud/em_glossary/em_glossary/-/boards) where initial definitions are drafted based on information from various sources in individual issues.
2. **Term implementation:** once discussions converge to a stable definition, a given term is implemented as a YAML-file via a [Merge Request](https://codebase.helmholtz.cloud/em_glossary/em_glossary/-/merge_requests). The implemented YAML file is required to conform to the [YAML template](https://codebase.helmholtz.cloud/em_glossary/em_glossary/-/blob/main/schema/template_new_term.yaml) and an unique IRI needs to be generated. Both are automatically checked with a CI/CD pipeline automatically. Merge requests often accumulate several terms from multiple issues in a semantic cluster - related issues are linked to a given merge request.
3. **Review and approval:** once all definitions and further details are generated, each merge request is reviewed in one of our bi-weekly meetings. Contributors approve merge requests after review - a minimum of 3 independent approvals are required for a term to be integrated into the EM Glossary. After review and approval, YAML files are merged to main, after which they can be found in the [terms folder](https://codebase.helmholtz.cloud/em_glossary/em_glossary/-/tree/main/terms).

### How you can contribute?
Would you like to contribute? Here are some ways you can work with us: 

1. **Suggest the development of a new term** by [creating an issue ](https://gitlab.hzdr.de/em_glossary/em_glossary/-/issues/new) on our board.
2. **Join our mailing list** by writing an email to hmc@fz-juelich.de or hmc-matter@helmholtz-berlin.de. Via our mailinglist we distribute updates regarding the development process and invitations to our bi-weekly meetings.
3. **Join our meetings** - we meet bi-weekly to discuss issues and merge requests in this repository.
4. **Request full access to this repository** - even though all development is open and fully transparent, some features (such as approving a merge request) are available for members only.

Additional information and a more detailed description of the workflow can be found in the [wiki](https://gitlab.hzdr.de/em_glossary/em_glossary/-/wikis/home).

### Re-use of the content in this repository
:warning: The content in this repository is for development purpose only and not intended for direct re-use (therefore the content in this repository is not licensed).:warning:

:link: However, we **encourage the re-use of terminology from the EM Glossary** in the following ways:
- visit the [EM Glossary Explorer](emglossary.helmholtz-metadaten.de) to explore glossary terms. Re-use and cite cite us in your textual works, using the details given on each individual term page.
- for technical implementation in your metadata schema or ontology, please re-use the OWL implementation of the EM Glossary. EMG.owl can be dereferenced on a class basis via our base URI ["purls.helmholtz-metadaten.de/emg"]([purls.helmholtz-metadaten.de/emg]). The OWL file is maintained in our [EM Glossary OWL](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl) repository. Guidance and suggestions for re-use and technical implementation can be found in our [EM Glossary OWL wiki](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/wikis/home).

:envelope: Should you have further questions or suggestions, please do not hesitate to contact us via hmc@fz-juelich.de & hmc-matter@helmholtz-berlin.de or join our meetings.


### Scope of the EM-Glossary

Research data has to fulfil a number of fundamental requirements in order to make the most of it - today and in the future. The FAIR principles (Wilkinson, M.D. et al. (2016) Scientific Data. http://dx.doi.org/10.1038/sdata.2016.18) – findable, accessible, interoperable, and reusable - describe such dispositions for digital data, but the pathway to realisation is often unclear. Here we focus on the challenge of implementing the ‘I’ in FAIR. Interoperability is the potential for two independent agents to work on the same data in a coordinated fashion. Achieving interoperability of research data requires interoperable metadata. This raises problems for the researcher collecting metadata: how do I annotate data and document the way they have been generated so that another researcher understands what I mean? How do I disambiguate terms that are used in adjacent scientific disciplines but have differing meanings?

The use and adoption of established vocabularies and semantic standards can provide solutions – however these may be unfeasible to implement, non-existent, or scientifically unsuitable. These are some of the reasons why a number of initiatives are developing semantic artifacts that aim to describe experimental equipment, workflows, and analysis procedures in electron microscopies. Harmonisation of these efforts to ensure interoperability across the community is required. 

<img
  src="https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/wikis/uploads/606b4c874e7a03be3ffb1d5cef5ec969/EMGlossary_GlueTechnology.png"
  width="600"
  alt="EM-Glossary interoperabiltiy" />

To support the long-term semantic interoperability of these efforts the Helmholtz Metadata Collaboration (HMC) is coordinating a community-wide effort to create a joint resource that will harmonize semantics in the field of electron and ion microscopies. The *EM Glossary group* strives to achieve consensus on terms commonly used in electron and ion microscopies via a remote, collaborative workflow based on the platform GitLab. With these we will establishe a ressource which provides harmonized and machine actionable semantics to support specific development efforts such as metadata schemas or ontologies in their respective fields. This will benefit experimental scientists by providing clarity when annotating and re-using research data; and by facilitating communication when conducting interdisciplinary studies.

### Funding Acknowledgements
This work was supported by (1)) the [Helmholtz Metadata Collaboration (HMC)](www.helmholtz-metadaten.de), an incubator-platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative, specifically HMC Hub Information at the [Institute for Advanced Simulation - Materials Data Science and Informatics (IAS-9)](https://www.fz-juelich.de/de/ias/ias-9) at [Forschungszentrum Jülich GmbH (FZJ)](https://ror.org/02nv7yv05), and HMC Hub Matter at the  Abteilung Experimentsteuerung und Datenerfassung at the [Helmholtz-Zentrum Berlin für Materialien und Energie GmbH (HZB)](https://ror.org/02aj13c28), and (2) the [NFDI-MatWerk](https://nfdi-matwerk.de/) consortium funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under the National Research Data Infrastructure – NFDI 38/1 – project number 460247524.

<p align="center">
  <img alt="Logo_HMC" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/HMC/HMC_Logo_M.png?raw=true" width="300">
&nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Logo_MatWerk" src="https://nfdi-matwerk.de/fileadmin/_processed_/8/3/csm_Logo_NFDI-MatWerk-1000px__1__d04d7a77da.png" width="100">
&nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Logo_FZJ" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/FZJ/Logo_FZ_Juelich_412x120_rgb_jpg.jpg?raw=true" width="200">
&nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Logo_HZB" src="https://www.helmholtz-berlin.de/media/design/logo/hzb-logo.svg" width="200">
</p>

import os
import yaml

# Define the path to the directory containing the YAML files
directory_path = "../terms"

# Function to read YAML file and extract IRI and label
def extract_iri_label(file_path):
    with open(file_path, 'r') as file:
        content = yaml.safe_load(file)
        iri = content.get('iri')
        label = content.get('label')
        return {'iri': iri, 'label': label}

# Get a list of all YAML files in the directory
yaml_files = [f for f in os.listdir(directory_path) if f.endswith('.yaml')]

# Extract IRIs and labels from all files in the directory
iri_label_list = [extract_iri_label(os.path.join(directory_path, file)) for file in yaml_files]

# Process the list to get the last part of the IRI
processed_list = [{'iri': item['iri'].split('/')[-1], 'label': item['label']} for item in iri_label_list]

# Sort the list by the last part of the IRI
sorted_list = sorted(processed_list, key=lambda x: x['iri'])

# Print the sorted list
for item in sorted_list:
    print(f"{item['iri']}, {item['label']}")

# Define the output path
output_path = "currentIRIs.yaml"

# Save the sorted list to a YAML file
with open(output_path, 'w') as file:
    yaml.dump(sorted_list, file)

print(f"Output saved to {output_path}")
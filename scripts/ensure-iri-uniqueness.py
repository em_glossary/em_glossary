#!/usr/bin/env python3

import yaml
import re
from pathlib import Path
from itertools import chain

# This can be used once we agree on the format of the IRIs
# IRI_PATTERN = re.compile("https:\/\/iris\.hmc-services\.de\/term_[0-9]{8}")
REPOSITORY_DIR = Path().resolve()
TERM_DIR = REPOSITORY_DIR / "terms/"
EXIT_STATUS = 0

def print_error(msg: str):
    print("{}\n---------".format(msg))

class DuplicatedIRI(Exception):
    def __init__(self, iri, original_file):
        super().__init__(f"Duplicate IRI found: {iri} ({original_file})")

class IRIDict(dict):
    def add(self, iri, file_name):
        if iri in self:
            raise DuplicatedIRI(iri, self[iri])
        self[iri] = file_name

IRI_SET = IRIDict()
for term_file in chain(TERM_DIR.glob("*.yaml"), TERM_DIR.glob("*.yml")):

    with open(term_file, "r") as yaml_file:
        try:
            term = list(yaml.safe_load_all(yaml_file))[0] #  load term
            try:
                # This can be used once we agree on the format of the IRIs:

                # # validate if IRI is well-formed
                # if not re.fullmatch(IRI_PATTERN, term["iri"]):
                    # print_error("[INVALID IRI] {}: {}".format(term_file, term["iri"]))
                    # EXIT_STATUS = 1
                # validate IRI uniqueness
                IRI_SET.add(term["iri"], term_file)

            except DuplicatedIRI as e:
                print_error(f"[DUPLICATE] {term_file}: {e}")
                EXIT_STATUS = 1

            except KeyError:
                print_error(f"[NO IRI] {term_file}: no IRI defined")
                EXIT_STATUS = 1

        except yaml.YAMLError as e:
            print_error(f"[Error] {term_file}: Could not load YAML file\n{e}")
            EXIT_STATUS = 1

exit(EXIT_STATUS)

#!/usr/bin/env python3

import yaml
import json
from jsonschema import Draft7Validator, ValidationError
from pathlib import Path
from itertools import chain


REPOSITORY_DIR = Path().resolve()
SCHEMA_FILE = REPOSITORY_DIR / "schema/termSchema.json"
TERM_DIR =  REPOSITORY_DIR/ "terms"
EXIT_STATUS = 0


with open(SCHEMA_FILE, 'r') as schema_file:
    termSchema = json.load(schema_file) #  load json-schema
    validator = Draft7Validator(termSchema)

    for term_file in chain(TERM_DIR.glob("*.yaml"), TERM_DIR.glob("*.yml")):

        with open(term_file, "r") as yaml_file:
            try:
                term = list(yaml.safe_load_all(yaml_file)) #  load term
                try:
                    validator.validate(term[0]) #  validate term

                except ValidationError as e:
                    print(f"Term definition is not valid: {term_file}.\n{e}\n\n")
                    EXIT_STATUS = 1    

            except yaml.YAMLError as e:
                print(f"Could not load YAML file: {term_file}\n\n")
                EXIT_STATUS = 1

exit(EXIT_STATUS)

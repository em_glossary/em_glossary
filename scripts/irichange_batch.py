#import yaml
#from ruamel.yaml import YAML
import ruamel.yaml
yaml = ruamel.yaml.YAML()  
import os
import glob

dir = '../terms/'
files = sorted(glob.glob(dir+'*.yaml'))
nfiles = len(files)


baseurl = 'https://purls.helmholtz-metadaten.de/emg/EMG_000000'
iriext = list(range(1,nfiles+1))
newiris = [baseurl+str(i).zfill(2) for i in iriext]
#len(newiris)


for i, j in zip(files, newiris):
    with open(i, 'r') as file:
        currFile = yaml.load(file)
        # currFile = ruamel.yaml.load(file, Loader=ruamel.yaml.RoundTripLoader)
        currFile['iri'] = j
    # new_file_name = 'terms/test_file1.yaml'
    with open(i, 'w') as file:
        yaml.dump(currFile, file)
        #ruamel.yaml.dump(currFile, Dumper=ruamel.yaml.RoundTripDumper)
    #print(i,j)